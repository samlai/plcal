# Programmable calendar

It's a software to create  a programable calendar.

The information will be pleced in files witch prolog-like syntax.

The event is a clausule like:

```prolog
birth(adam,date(2005,5,23)).

birthday(Person,date(NextYears,Month,DayOfMonth)) :-
	birth(Person,date(Year,Month,DayOfMonth)),
	succ(Year,NextYear),
	plus(Year,150,EndYear),
	between(NextYear,EndYear,NextYears).
```

And presented by:

```prolog
:- add_event birth/2.
:- set_event(birth(Person,_),title(format("Birth of ~w"),[Person])).
:- set_event(birthday(Person,_),title(format("Birthday of ~w"),[Person])).

:- set_event(birth(_,Day),day(Day)).
:- set_event(birthday(_,Day),day(Day)).

:- set_event(birth/2,color(yellow)).
:- set_event(birthday/2,color(gold)).
```

To see your week in terminal:

```
?- week.
```

The same effect using `days(8)`.

To export 360 days to `ical` format:

```
?- ical(today,360).
```

You can also reply ical by http like `reply_http_page/2`

```
?- reply_ical(today,360).
```
