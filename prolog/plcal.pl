:- module(plcal,[
              %calendar/0,
              start_events/0,
              end_events/0,
              (set_event)/1,
              (set_event)/2,
              (add_event)/1,
              %week/0,
              %days/1,
              reply_ical/2,
              ical/2,
              ical_stream/3,
              op(1150,fx,add_event),
              op(1150,fx,set_event)
          ]).

%:- use_module(library(ical/core)).
:- use_module(library(date_time)).
%:- use_module(library(julian)).

:- use_module(library(dcg/basics)).

:- dynamic event_data/2.
:- dynamic event_type/1.
:- dynamic event_day/2.

start_events:-
    abolish(event_data/2),
    abolish(event_type/1),
    abolish(event_day/2).

end_events:-
    compile_predicates([event_data/2,event_type/1,event_day/2]).

set_event(B):-
    just_added(W),
    set_event(W,B).

:- meta_predicate set_event(:,+).

set_event(M:A/N,B):-
    functor(F,A,N),
    !,
    set_event(M:F,B).
set_event(W,date(D)):-
    !,
    assertz(event_day(W,D)).
set_event(W,days(D)):-
    !,
    assertz(event_day(W,D)).
set_event(W,K):-
    assertz(event_data(W,K)).

:- dynamic just_added/1.
:- volatile just_added/1.

:- meta_predicate add_event(:).

add_event(A):-
    assertz(event_type(A)),
    retractall(just_added/1),
    asserta(just_added(A)).

event_format_summry(Summary,Event):-
    event_data(Event,summary(SS,R)),
    format(Summary,SS,R).

/* useful: stamp_date_time/3, get_time/1 */
/*
calendar:-
    get_time(Time),
    stamp_date_time(Time,date(Y,M,D,_,_,_,_,_,_),local),
    succ(Y,YY),
    get_all_events(E,date(Y,M,D),date(YY,M,D)),
    nl,
    write_events(user_output,E).

week:-
    days(8).

days(N):-
    get_time(Time),
    stamp_date_time(Time,date(Y,M,D,_,_,_,_,_,_),local),
    days_(date(Y,M,D),N).

days_(_,0):-!.
days_(Day,N):-
    write_data(user_output,Day),
    findall(Day-Event,
            (event_type(Type),
             event_day(Type,Day)
            ),
            Events),
    write_events(user_output,Events),
    succ(NN,N),
    date_add(Day, 1 days,Next),
    days_(Next,NN).


get_all_events(Events,Start,End):-
    findall(TEvents,
            (event_type(Type),
             get_events(TEvents,Type,Start,End)
            ),
            ToMerge),
    merge_events_list(ToMerge,Events).

:- meta_predicate get_events(-,:,+,+).

get_events(Events,M:A/N,Start,End):-
    !,
    functor(F,A,N),
    get_events(Events,M:F,Start,End).
get_events(Events,Term,Start,End):-
    findall(Day-Term,
            (Term,
             event_day(Term,Day),
             Start @=<Day,
             Day @=< End
            ),
            NSEvents),
    sort(1,@=<,NSEvents,Events).
*/

%!  day_section(+Term,-Term)

day_section(day(Y,M,D),section(date(Y,M,D),DD)):-
    !,
    date_add(date(Y,M,D),1 days, DD).
day_section(date(Y,M,D),section(datetime(Y,M,D,0,0,0),DD)):-
    !,
    datetime_add(datetime(Y,M,D,0,0,0),1 secs, DD).
day_section(section(A,B),W):-
    !,
    W = section(A,B).
day_section([A],B):-
    !,
    day_section(A,B).
day_section([A|_],K):-
    day_section(A,K).
day_section([_|A],K):-
    day_section(A,K).

%!  event_on_days(Event,Type,Section)

:- meta_predicate event_on_days(:,+,-,+,+).

event_on_days(M:A/N,P,L,Start,End):-
    !,
    functor(F,A,N),
    arg(P,F,Day),
    event_on_days(M:F,Day,L,Start,End).

event_on_days(Event,Day,L,Start,End):-
    event_on_days_(Event,Day,L,[],Start,End,3).

event_on_days_(_:true,D,[D|R],R,_,_,_):-!.
event_on_days_(M:(A,B),Day,L,R,Start,End,Miss):-
    !,
    % TODO (A,K,L)
    throw(error(not_impolemented([A,L,K]))),
    event_on_days_(M:B,Day,K,R,Start,End,Miss).
event_on_days_(Module:Event,Day,L,R,Start,End,M):-
    !,
    findall(p(Day,Module:Body),catch((clause(Module:Event,Body),Event\=Body),_,fail),Clauses),
    event_on_days_list(Clauses,L,R,Start,End,M).
event_on_days_(Event,Day,L,R,Start,End,M):-
    days_reads(Event,Day,L,R,Start,End,M).

days_reads(Event,Day,List,R,Start,End,Miss):-
    setup_call_cleanup(
        engine_create(Day, Event, E),
        days_reads_(E, List,R,Start,End,Miss),
        engine_destroy(E)).

event_on_days_list([p(D,B)],L,R,Start,End,M):-
    !,
    event_on_days_(B,D,L,R,Start,End,M).
event_on_days_list([p(D,B)|T],L,R,Start,End,M):-
    event_on_days_(B,D,L,E,Start,End,M),
    event_on_days_list(T,E,R,Start,End,M).

days_reads_(_, R,R,_,_,0):-!.
days_reads_(E, [H|T],R,Start,End,Miss) :-
    engine_next(E, H), !,
    (   Start @=< H,
        H @=< End
    ->  M = Miss
    ;   succ(M,Miss)
    ),
    days_reads_(E,T,R,Start,End,M).
days_reads_(_,R,R,_,_,_).

%!  event_on_day(-Event,+Type,+Day)

:- meta_predicate event_on_date(-,:,+).

event_on_date(Event,M:A/N,Day):-
    !,
    functor(F,A,N),
    event_on_date(Event,M:F,Day).

event_on_date(Event,Event,Day):-
    event_day(Event,Day),
    Event.

merge_events_list([G|T],E):-
    merge_events_list_(G,T,E).

merge_events_list_(G,[],G):-!.
merge_events_list_(G,[A|T],E):-
    merge_events(G,A,R),
    merge_events_list_(R,T,E).

merge_events([],A,A):-!.
merge_events(A,[],A):-!.
merge_events([DA-EA|A],[DB-EB|B],[DA-EA|C]):-
    DA @< DB,
    !,
    merge_events(A,[DB-EB|B],C).
merge_events(A,[WB|B],[WB|C]):-
    merge_events(A,B,C).

%---- Terminal writing ----

write_data(S,date(Y,M,D)):-
    format_time(S,"(%a) %F:\n",date(Y,M,D,0,0,0,0,-,-)).

write_events(_,[]):-!.
write_events(S,[Day-FF|T]):-
    write_color(S,FF),
    event_data(FF,terminal_format(SS,R)),
    !,
    write_data(S,Day),
    format(S,SS,R),
    write_picture(S,FF),
    format(S,"\e[0m",[]),
    write_events(S,T).
write_events(S,[date(Y,M,D)-E|T]):-
    format(S,"~d-~d-~d:\n~w\n\n",[Y,M,D,E]),
    write_events(S,T).


write_color(S,F):-
    event_data(F,terminal_color(K)),
    !,
    format(S,K,[]).
write_color(_,_).

write_picture(S,F):-
    event_data(F,terminal_picture(K,_)),
    !,
    format(S,K,[]).
write_picture(_,_).


%---- ICal generator ----

reply_ical(Day,N) :-
    format("Content-Type: text/html; charset=UTF-8\r\n",[]),
    format("\r\n",[]),
    ical_stream(current_output,Day,N).

ical(Day,N):-
    ical_stream(current_output,Day,N).

ical_stream(Stream,Day,N):-
    ical_stream_(Day,N,Events),
    phrase(ical_object(object(`VCALENDAR`,
                              Events))
          ,LL),
    string_codes(SS,LL),
    write(Stream,SS).
/*
ical_stream_(_,0,[]):-!.
ical_stream_(Date,N,Events):-
    date_add(Date,1 days,Next),
    findall(object(`VEVENT`,
                   [content_date(`DTSTART`,Date),
                    content_date(`DTEND`,Next),
                    content(`SUMMARY`,Codes)]),
            (event_type(Type),
             event_on_date(Event,Type,Date),
             event_format_summry(codes(Codes),Event)
            ),
            Events,
            Tail),

    succ(NN,N),
    ical_stream_(Next,NN,Tail).
*/

ical_stream_(_Date,_N,Events):-
    %date_add(Date,N days,Next),
    findall(object(`VEVENT`,
                   [content_date(`DTSTART`,Start),
                    content_date(`DTEND`,End),
                    content(`SUMMARY`,Codes)]),
            (event_type(Type),
             event_on_date(Event,Type,Days),
             day_section(Days,section(Start,End)),
             event_format_summry(codes(Codes),Event)
            ),
            Events).


contents([]) --> [].
contents([H|T]) -->
    content_line(H),
    !,
    contents(T).
contents([H|T]) -->
    ical_object(H),
    contents(T).

ical_object(object(Type,Body)) -->
    "BEGIN:",string(Type),"\r\n",
    contents(Body),
    "END:",string(Type),"\r\n".

content_line(content_date(Name,datetime(Y,L,D,H,M,S))) -->
    content_line(content_date(Name,date(Y,L,D,H,M,S,0,-,-))).
content_line(content_date(Name,Date)) -->
    {ical_date_format(codes(CC),Date)},
    string(Name), ":",string(CC),"\r\n".
content_line(content(Name,Val)) -->
    string(Name), ":", string(Val),"\r\n".
content_line(content(Name,Params,Val)) -->
    string(Name), ical_content_params(Params),":", string(Val),"\r\n".

ical_content_params([]) --> "".
ical_content_params([V=A|T]) -->
    ";",string(V),"=",string(A),
    ical_content_params(T).

ical_date_format(Output,date(Y,M,D)):-
    format_time(Output,"%Y%m%d",date(Y,M,D)).

ical_date_format(Output,Date):-
    format_time(Output,"%Y%m%dT%H%m%SZ",Date).


















